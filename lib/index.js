var addon = require('../native');

module.exports = {
    block: addon.block,
    inline: addon.inline,
};