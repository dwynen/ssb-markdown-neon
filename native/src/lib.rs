use neon::prelude::*;


fn block_md(mut cx: FunctionContext) -> JsResult<JsString> {
    let text = cx.argument::<JsString>(0)?;
    let parser = &mut markdown_it::MarkdownIt::new();

    markdown_it::plugins::cmark::add(parser);
    markdown_it::plugins::extra::add(parser);

    let ast  = parser.parse(&text.value());
    let html = ast.render();

    Ok(cx.string(html))
}

fn inline_md(mut cx: FunctionContext) -> JsResult<JsString> {
    // for now, just return the same as block_md(), until we have *something*
    // working
    block_md(cx)
}

register_module!(mut cx, {
    cx.export_function("block", block_md)?;
    cx.export_function("inline", inline_md)?;
    Ok(())
});
